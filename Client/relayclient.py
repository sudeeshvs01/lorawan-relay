import paho.mqtt.client as mqtt
import time
import threading
import ssl
import random
import uuid
import codecs
import base64
from time import sleep
import json
import random
from datetime import datetime
#runs in virtualgateway
receivedMessages = []
frequency = ['865402500', '865062500', '865985000']
channel = ['1', '0', '2']
pubtopic="gateway/1f8b3d294449d816/event/up"

def gatewaystat(data='QAMAACYAMwEC1oVn+11kb/RJ6XSEP9n+aX+cjYI='):
    uplinkID = uuid.uuid4().hex
    uplinkID = codecs.encode(codecs.decode(uplinkID, 'hex'), 'base64').decode()
    uplinkID = uplinkID.strip()
    print( uplinkID)
    freq = random.choice(frequency)
    ind = frequency.index(freq)
    cha = channel[ind]
    print(freq, ",", cha)
    now = datetime.now()
    date = now.strftime("%Y-%m-%dT%H:%M:%SZ")
    msg='{"phyPayload":"'+data+'","txInfo":{"frequency":'+freq+',"modulation":"LORA","loRaModulationInfo":{"bandwidth":125,"spreadingFactor":7,"codeRate":"4/5","polarizationInversion":false}},"rxInfo":{"gatewayID":"H4s9KURJ2BY=","time":null,"timeSinceGPSEpoch":null,"rssi":-73,"loRaSNR":8.8,"channel":' + \
        cha+',"rfChain":1,"board":0,"antenna":0,"location":null,"fineTimestampType":"NONE","context":"q4eeww==","uplinkID":"'+str(uplinkID)+'","crcStatus":"CRC_OK"}}'         
    return msg


def on_connect(client, a, b, c):
    print("connected")
    client.subscribe("application/108/device/1c1c877c7b596342/event/up")
def on_connect2(client, a, b, c):
    print("connected2")
  


def on_message(client, userdata, message):
    da=json.loads(message.payload)
    frame=json.loads(da['payload'])
    frame=gatewaystat(frame['data'])
    client2.publish(pubtopic,frame, 2)


def on_publish(client, userdata, mid):
    print("published")


# connect the client to Cumulocity IoT and register a device

client2 = mqtt.Client("relaynode_client2")
client2.on_publish = on_publish
client2.on_message = on_message
client2.on_connect = on_connect
client2.username_pw_set("relay", "icfoss@1234")
client2.connect("localhost",11883)
client2.loop_forever()
