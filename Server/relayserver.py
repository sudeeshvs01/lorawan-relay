import paho.mqtt.client as mqtt
import time
import json
import os
import toml
import base64
import subprocess
# code runs in remote gateway
config = toml.load("/home/pi/relay/relay.toml")
appid = config['relay']['appid']
deveui = config['relay']['deviceeui']


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe(config['mqtt']['subtopic'])


def on_message(client, userdata, message):
    data = str(message.payload.decode("utf-8"))
    print(data)
    data = json.loads(data)
    data1 = str(data["phyPayload"])
    cmd = 'lora-packet-decode --base64 '+data1
    proc = subprocess.Popen([cmd], stdout=subprocess.PIPE, shell=True)
    out = proc.stdout.read()
    out = out.decode("utf8")
    ind1 = out.index("FOpts")
    ind2 = out.index("DevAddr", ind1)
    ind3 = out.index("(Big")
    out = out[ind2: ind3]
    out = out.split("=")[1]
    out = out.strip()
    if out == config['relay']['deviceid']:
        print("relay node")

    else:
        print("forward node")
        print(data1)
        if len(data1) <= 50:
            message = base64.b64encode(data1.encode("utf8")).decode("utf8")
            updata = {"confirmed": False, "fPort": 10, "data": message}
            topic = "application/"+appid+"/device/"+deveui+"/command/down"
            client.publish(topic, json.dumps(updata))


broker_address = config['mqtt']['server']
client = mqtt.Client("P1")
client.on_message = on_message
client.on_connect = on_connect
client.connect(broker_address)
client.loop_forever()
